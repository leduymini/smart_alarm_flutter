import 'package:flutter/material.dart';
import 'dart:io' show Platform;
import 'clock.dart';
import 'createAlarmScreen.dart';

void main() => runApp(MyApp());

final List<String> alarmList = <String>[
  '6:15 AM',
  '13:30 PM',
  '13:45 PM',
  '13:45 PM',
  '13:45 PM',
  '13:45 PM',
  '13:45 PM',
  '13:45 PM'
];
// final List<String> alarmList = <String>[];
final List<int> colorCodes = <int>[600, 500, 100];

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(title: 'Alarm Smart Clock', home: HomeScreen());
  }
}

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() {
    return _HomeScreenState();
  }
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Alarm Smart Clock'),
      ),
      body: Center(
          child: ListView.builder(
        padding: EdgeInsets.all(10),
        physics: Platform.isAndroid
            ? ClampingScrollPhysics()
            : BouncingScrollPhysics(),
        itemCount: alarmList.length + 2,
        itemBuilder: (BuildContext context, int index) {
          if (index == 0) return ClockUI();
          if (index == 1)
            return Padding(
                padding: EdgeInsets.only(bottom: 10, top: 20),
                child: Text('Your Alarms'));
          index -= 2;
          if (alarmList.length == 0)
            return Container(
                margin: EdgeInsets.only(bottom: 10),
                decoration: alarmBoxDecoration(),
                height: 100,
                child: Center(child: Text('Chưa có báo thức')));
          else if (alarmList.length > 0)
            return Container(
                margin: EdgeInsets.only(bottom: 10),
                decoration: alarmBoxDecoration(),
                height: 100,
                child: InkWell(
                    onTap: () => (print('Choose Screen')),
                    child: Container(
                        // color: Colors.amber[colorCodes[index]],
                        child: Center(child: Text('${alarmList[index]}')))));
        },
      )),
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => CreateAlarm()),
            );
          },
          child: Icon(Icons.add)),
    );
  }
}

BoxDecoration alarmBoxDecoration() {
  return BoxDecoration(
      border: Border.all(),
      borderRadius: BorderRadius.all(Radius.circular(10)));
}
